Herramienta para convertir los pasos con los que suelo empezar una maquina en una CTF y mas adelante poner opciones para maquina en RL(real life) la idea es que todo corra y vaya dando informacion para poder realizar varias tareas a la vez o simplemente dejarlo corriendo y hacer otras cosas, se usa la API/Libs de nmap, pero de momento gobuster/nikto etc se le llama desde GO, pero se cambiara para que sea natural del lenguage poco a poco, para poder usar el poder que tiene GO en herramientas de hackig que es MUCHA.

ejemplo:
```
./chikienumerator -t=10.10.10.146 -x=php,html,txt,sh
```
ayuda:
```
./chikienumerator -h
```

necesario:
- nmap
- nikto
- Gobuster
- BlackArch GNU/Linux ( recomendado pero cualquier GNU/Linux sobra )


A que es equivale este programa de momento:
1. nmap -sC -sV -oA target
2. nmap -script="http-* and not brute" target
3. nikto -h target
4. Gobuster
