package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/Ullaakut/nmap"
)

func crearSkeleto(targetHosts, targetDir string) {

	fmt.Printf("\tCreando proyecto en: %s\n", targetDir)
	if _, err := os.Stat(targetDir); os.IsNotExist(err) {
		err = os.MkdirAll(targetDir, 0755)
		if err != nil {
			panic(err)
		}
	}
}

func runNmap(targetHosts, targetDir string) []uint16 {
	var puertosHttp []uint16

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Minute)
	defer cancel()

	// esto es lo equivalente a:
	// nmap -sC -sV  127.0.0.1
	scanner, err := nmap.NewScanner(
		nmap.WithTargets(targetHosts),
		nmap.WithDefaultScript(),
		nmap.WithServiceInfo(),
		nmap.WithMostCommonPorts(1000),
		nmap.WithSkipHostDiscovery(),
		nmap.WithContext(ctx),
		nmap.WithTimingTemplate(nmap.TimingAggressive),
	)
	if err != nil {
		log.Fatalf("unable to create nmap scanner: %v", err)
	}

	fmt.Println("Esto puede tardar un ratico...")

	result, err := scanner.Run()
	if err != nil {
		log.Fatalf("el escan de nmap fallo: %v", err)
	}

	for _, host := range result.Hosts {
		fmt.Printf("Host %s\n", host.Addresses[0])

		for _, port := range host.Ports {

			format := fmt.Sprintf("\tPuerto %d/%s %s %s %s %s %s\n", port.ID, port.Protocol, port.State, port.Service.Name, port.Service.Product, port.Service.Version, port.Service.ExtraInfo)

			fmt.Println(format)
			escribirFichero(format, targetHosts, targetDir)
			for _, script := range port.Scripts {
				out := fmt.Sprintf("\t%s\n", strings.Replace(script.Output, "\n", "\n\t\t\t", -1))
				escribirFichero(out, targetHosts, targetDir)
				fmt.Println(out)
			}
			// Limpiar variable por si acaso
			servicio := strings.TrimRight(port.Service.Name, "\n")
			// Convertir nmap.State a string
			estado := fmt.Sprintf("%s", port.State)
			// anadir puerto a slice si el protocolo es http
			// esto crecera para mas servicios aparte de http/s
			// con switch or case statemens https://www.callicoder.com/golang-control-flow/#switch-statement
			if servicio == "http" || servicio == "https" || port.ID == 80 || port.ID == 443 || port.ID == 8080 {
				if estado != "closed" {
					fmt.Printf("Adding port %d to array \n", port.ID)
					fmt.Printf("El estado del puerto es %s \n", estado)
					puertosHttp = append(puertosHttp, port.ID)
				}
			}
		}
	}

	return puertosHttp
}

func runNikto(targetHosts, targetDir string, puertosHttp []uint16) {

	fmt.Printf("\n\t+- Corriendo Kikto\n")
	puertosString := convertir(puertosHttp)
	fichero := targetDir + "/" + targetHosts

	for i := range puertosString {
		puerto := puertosString[i]
		cmd := exec.Command("nikto", "-nointeractive", "-host", targetHosts, "-port", puerto)
		outfile, error := os.OpenFile(fichero, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if error != nil {
			panic(error)
		}
		defer outfile.Close()
		// cmd.Stdout = os.Stdout
		cmd.Stdout = outfile
		cmd.Stderr = os.Stderr
		fmt.Printf("corriendo:\n \t%s\n", cmd)
		err := cmd.Run()
		if err != nil {
			log.Fatalf("cmd.Run() termino con error  %s\n", err)
		}

	}

}

func runGobuster(targetHosts, targetDir string, puertosHttp []uint16, fileExt, directoryList string) {

	fmt.Printf("\n\t+- Corriendo Gobuster para encontrar ficheros y directorios ocultos\n")
	puertosString := convertir(puertosHttp)
	fichero := targetDir + "/" + targetHosts
	var target string
	for i := range puertosString {
		puerto := puertosString[i]
		if puerto == "443" {
			target = "https://" + targetHosts + ":" + puerto
		} else {
			target = "http://" + targetHosts + ":" + puerto
		}
		cmd := exec.Command("gobuster", "dir", "-w", directoryList, "-u", target, "-x", fileExt)
		outfile, error := os.OpenFile(fichero, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if error != nil {
			panic(error)
		}
		defer outfile.Close()

		// cmd.Stdout = os.Stdout
		cmd.Stdout = outfile
		cmd.Stderr = os.Stderr
		fmt.Printf("corriendo:\n \t%s\n", cmd)
		err := cmd.Run()
		if err != nil {
			log.Fatalf("cmd.Run() termino con error %s\n", err)
		}

	}

}

func scanHttp(targetHosts, targetDir string, puertosHttp []uint16) {
	fmt.Printf("\n\t+- VAMOS A CORRER PRUEBAS DE HTTP - Puede tardar mas de 10-15m\n")
	fmt.Printf("\n\t+- Ve y pegate una paja o empieza a explorar manualmente con la informacion anterior:\n")

	puertosHttpStr := strings.Join(convertir(puertosHttp), ",")

	//ctx, cancel := context.WithTimeout(context.Background(), 25*time.Minute)
	//defer cancel()

	escanearHttp, err := nmap.NewScanner(
		nmap.WithTargets(targetHosts),
		nmap.WithPorts(puertosHttpStr),
		nmap.WithScripts("http-* and not brute"),
		nmap.WithServiceInfo(),
		//	nmap.WithContext(ctx),
		nmap.WithTimingTemplate(nmap.TimingAggressive),
	)
	if err != nil {
		log.Fatalf("no se pudo crear el escanner de nmap: %v", err)
	}

	fmt.Printf("\n\t+- Probando puertos HTTP encontrados\n")

	resultado, err := escanearHttp.Run()
	if err != nil {
		log.Fatalf("el escan de nmap fallo: %v", err)
	}

	for _, host := range resultado.Hosts {
		fmt.Printf("Host %s\n", host.Addresses[0])

		for _, port := range host.Ports {
			format := fmt.Sprintf("\tPuerto %d/%s %s %s %s %s %s\n", port.ID, port.Protocol, port.State, port.Service.Name, port.Service.Product, port.Service.Version, port.Service.ExtraInfo)
			escribirFichero(format, targetHosts, targetDir)
			fmt.Println(format)

			for _, script := range port.Scripts {
				out := fmt.Sprintf("\t%s\n", strings.Replace(script.Output, "\n", "\n\t\t\t", -1))
				escribirFichero(out, targetHosts, targetDir)
				fmt.Println(out)

			}

		}
	}
}

func convertir(puertosHttp []uint16) []string {
	puertosString := []string{}

	for i := range puertosHttp {
		viejos := puertosHttp[i]
		convertir := strconv.FormatUint(uint64(viejos), 10)
		puertosString = append(puertosString, convertir)
	}
	return puertosString
}

func escribirFichero(toWrite, targetHosts, targetDir string) {
	log := targetDir + "/" + targetHosts

	f, err := os.OpenFile(log, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	l, err := f.WriteString(toWrite)
	if err != nil {
		fmt.Println(err)
		fmt.Println(l)
		f.Close()
		return
	}
	/* DEBUG */
	/*	fmt.Println(l, "[DEBUG] Escribiendo resultado al log") */
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

}

func checkExeExists(exe string) {
	path, err := exec.LookPath(exe)
	if err != nil {
		log.Fatalf("didn't find '%s' executable\n", exe)
		os.Exit(1)
	}
	fmt.Printf("'%s' executable is '%s'\n", exe, path)
	return
}

func main() {

	var targetHosts string
	var fileExt string
	var directoryList string
	var proyectoDir string

	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("Error al encontrar el directorio del usuario")
	}

	flag.StringVar(&targetHosts, "t", "127.0.0.1", "Poner los host/s separado por comillas")
	flag.StringVar(&fileExt, "x", "php,txt,html", "Extensiones de ficheros a buscar por ejemplo php,txt,html,sh")
	flag.StringVar(&directoryList, "d", "directory-list-1.0.txt", "Fichero con la lista de nombres de ficheros y directorios a buscar")
	flag.StringVar(&proyectoDir, "p", "hackthebox", "Nombre del proyecto como hackthebox o chikimikis etc")

	flag.Parse()
	if len(os.Args) < 1 {
		os.Exit(1)
	}

	// Tenemos instalados las herramientas?
	checkExeExists("nmap")
	checkExeExists("nikto")
	checkExeExists("gobuster")

	// crear proyecto
	targetDir := home + "/" + proyectoDir + "/" + targetHosts

	crearSkeleto(targetHosts, targetDir)

	puertosHttp := runNmap(targetHosts, targetDir)

	if len(puertosHttp) > 0 {
		scanHttp(targetHosts, targetDir, puertosHttp)
		runNikto(targetHosts, targetDir, puertosHttp)
		runGobuster(targetHosts, targetDir, puertosHttp, fileExt, directoryList)
	}
	// TODO escanear por fallos/info smb si se encontraron puertos 445 etc abiertos
	// TODO escanear por fallos/info dns si se encontraron puertos 53 tcp/udp abiertos
	// TODO escanear por fallos/info ...
}
